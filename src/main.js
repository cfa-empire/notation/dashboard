import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import config from './config';
import axios from 'axios'
import VueAxios from 'vue-axios'
import Service from './service';
 
Vue.use(VueAxios, axios);

import 'bootstrap'; 
import 'bootstrap/dist/css/bootstrap.min.css';
import './main.scss';
// import './second.css';

import dashboard from './components/Dashboard.vue';
import default_layout from './components/Layout.vue';
import Support from './components/Support.vue';

Vue.component('dashboard-layout', dashboard );
Vue.component('dashboard-layout', dashboard );
Vue.component('support', Support );

Vue.config.productionTip = false

Vue.prototype.$service = Service(config, axios);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
