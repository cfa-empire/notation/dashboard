import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/rates',
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../components/Login.vue')  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../components/Register.vue')
  },
  {
    path: '/configservice',
    name: 'configservice',
    component: () => import('../components/ConfigService.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/updateconfigservice',
    name: 'updateconfigservice',
    component: () => import('../components/UpdateConfigService.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/qrcode',
    name: 'qrcode',
    component: () => import('../components/Qrcode.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/rates',
    name: 'rates',
    component: () => import('../components/Rates.vue') ,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/beaud',
    name: 'beaud',
    component: () => import('../components/Dashboard.vue'),
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!localStorage.getItem('token')) {
      next({path: '/login'})
    } else {
      next();
    }
  } else {
    next();
  }
})

export default router
