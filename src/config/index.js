export default {
  apiUrl: process.env.NODE_ENV === "production" ? "https://manote-api.herokuapp.com/api/" : "http://localhost:5353/api/",
}