
function Service (config, axios) {

  const getToken = () => {
    return localStorage.getItem('token');
  };
 
  const get = (endpoint, params, callback) => {
    axios({
      method: 'get',
      url: config.apiUrl + endpoint,
      params
    }).then(function (response) {
      if (response.status == 200) {
        callback(null, response.data);
      } else {
        callback(response.data);
      }
    }, function (error) {
      return callback(error.response);
    });
  };

  const post = (endpoint, params, callback) => {
    axios({
      method: 'post',
      url: config.apiUrl + endpoint,
      data: params
    }).then(function (response) {
      if (response.status == 200) {
        callback(null, response.data);
      } else {
        callback(response.data);
      }
    }, function (error) {
      return callback(error.response);
    });
  };
  
  const getLogged = (endpoint, params, callback) => {
    const userToken = getToken();
    axios({
      method: 'get',
      url: config.apiUrl + endpoint,
      headers: { 'Authorization': "bearer " + userToken}, 
      params
    }).then(function (response) {
      if (response.status == 200) {
        callback(null, response.data);
      } else {
        callback(response.data);
      }
    }, function (error) {
      return callback(error.response);
    });
  };
  
  const postLogged = (endpoint, params, callback) => {
    const userToken = getToken();
    axios({
      method: 'post',
      url: config.apiUrl + endpoint,
      headers: { 'Authorization': "bearer " + userToken}, 
      data: params
    }).then(function (response) {
      if (response.status == 200) {
        callback(null, response.data);
      } else {
        callback(response.data);
      }
    }, function (error) {
      return callback(error.response);
    });
  };

  const putLogged = (endpoint, params, callback) => {
    const userToken = getToken();
    axios({
      method: 'put',
      url: config.apiUrl + endpoint,
      headers: { 'Authorization': "bearer " + userToken}, 
      data: params
    }).then(function (response) {
      if (response.status == 200) {
        callback(null, response.data);
      } else {
        callback(response.data);
      }
    }, function (error) {
      return callback(error.response);
    });
  };


  /*
  const testApi = (params, callback) => {
    get('test-api', params, callback);
  }*/

  const getMe = (callback) => {
    getLogged('users', {}, callback);
  }

  const register = (user, callback) => {
    post('users', user, callback);
  }

  const login = (user, callback)=> {
    post('users/login',user,callback);
  }

  const configserviceCreate = (service, callback)=>{
    postLogged('services/create',service, callback);
  }

  const configserviceUpdate = (service, callback)=>{
    putLogged('services/update',service, callback);
  }

  const getServiceByUser = (params, callback) => {
    getLogged('services/listByUser', params, callback);
  }

  const getServiceById = (id, callback) => {
    getLogged('services', { id }, callback);
  }

  const getRatesByService = (params, callback) => {
    getLogged('rates/service', params, callback);
  }

  return {
    //testApi,
    getMe,
    getServiceById,
    register,
    login,
    configserviceCreate,
    configserviceUpdate,
    getServiceByUser,
    getRatesByService,
  };
}

export default Service;